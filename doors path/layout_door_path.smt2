;; blocks = [1, 2, 3]
(set-logic QF_LRA)
(set-option :produce-models true)

;; variable: cost
(declare-fun cost () Real)

;; variable: delta
(declare-fun delta () Real)

;; variable: separation
(declare-fun separation () Real)

;;parameters
(declare-fun ROOM() Real) ;;lato stanza
(declare-fun TABLE() Real) ;;lato blocchetto
(declare-fun DOORSIDE() Real) ;;dimensione porta
(declare-fun ACC() Real) ;; accessibility

;;TABLES
;; variable: block coordinate
(declare-fun x_1 () Real)
(declare-fun y_1 () Real)
(declare-fun x_2 () Real)
(declare-fun y_2 () Real)
(declare-fun x_3 () Real)
(declare-fun y_3 () Real)
;; variable: block dimensions
(declare-fun dx_1 () Real)
(declare-fun dy_1 () Real)
(declare-fun dx_2 () Real)
(declare-fun dy_2 () Real)
(declare-fun dx_3 () Real)
(declare-fun dy_3 () Real)

;; DOORS
;; variable: coordinates
(declare-fun door_x_1 () Real)
(declare-fun door_y_1 () Real)
(declare-fun door_x_2 () Real)
(declare-fun door_y_2 () Real)
;; variable: dimensions
(declare-fun door_dx_1 () Real)
(declare-fun door_dy_1 () Real)
(declare-fun door_dx_2 () Real)
(declare-fun door_dy_2 () Real)

;;PATH
;; variable: coordinates
(declare-fun pathx_1 () Real)
(declare-fun pathy_1 () Real)
(declare-fun pathx_2 () Real)
(declare-fun pathy_2 () Real)
;; variable: dimensions
(declare-fun pathdx_1 () Real)
(declare-fun pathdy_1 () Real)
(declare-fun pathdx_2 () Real)
(declare-fun pathdy_2 () Real)

;; distances
(declare-fun DX_1_2 () Real)
(declare-fun DX_1_3 () Real)
(declare-fun DX_2_3 () Real)
(declare-fun DY_1_2 () Real)
(declare-fun DY_1_3 () Real)
(declare-fun DY_2_3 () Real)
(declare-fun sum_DX () Real)
(declare-fun sum_DY () Real)

;;DEBUG
;;to which door the path block is fitting
(declare-fun door_1_top () Bool)
(declare-fun door_1_right () Bool)
(declare-fun door_1_left () Bool)
(declare-fun door_1_bottom () Bool)
(declare-fun door_2_top () Bool)
(declare-fun door_2_right () Bool)
(declare-fun door_2_left () Bool)
(declare-fun door_2_bottom () Bool)
;;path touching way
(declare-fun touching_1_2_above () Bool)
(declare-fun touching_1_2_below () Bool)
(declare-fun touching_1_2_leftup () Bool)
(declare-fun touching_1_2_leftdown () Bool)
;;ordering
(declare-fun ordered_1_2 () Bool)


;; problem
(assert
   (and

       ;; constraint: hack
       (= ROOM 1)
       (= TABLE (/ 3602879701896397 18014398509481984))
       (= DOORSIDE (/ 3602879701896397 18014398509481984))
       (= ACC (/ 3602879701896397 72057594037927936))

       ;; constraint: fixed doors
       ;; dimensions
       (= door_dx_1 DOORSIDE)
       (= door_dy_1 0)
       (= door_dx_2 0)
       (= door_dy_2 DOORSIDE)
	   ;;coordinates
       (= door_x_1 (/ 1 3)) 
       (= door_y_1 1)      
       (= door_x_2 1)
       (= door_y_2 (/ 1 3))
       ;; bounding box
       (>= pathx_1 0) (<= pathx_1 ROOM) (>= pathdx_1 0) (<= pathdx_1 ROOM)
       (>= pathy_1 0) (<= pathy_1 ROOM) (>= pathdy_1 0) (<= pathdy_1 ROOM)
       (>= pathx_2 0) (<= pathx_2 ROOM) (>= pathdx_2 0) (<= pathdx_2 ROOM)
       (>= pathy_2 0) (<= pathy_2 ROOM) (>= pathdy_2 0) (<= pathdy_2 ROOM)

       ;; contraints: side of first block fit one door, side of the second block must fit the other door

       ;; the first block of the path must fit the first door
	   ;; the door is on the top wall
	   ( = door_1_top (and
               (= pathx_1 door_x_1)
               (= (+ pathy_1 pathdy_1) door_y_1)
               (= pathdx_1 door_dx_1)))
		;; the door is on the bottom wall
		( = door_1_bottom (and
               (= pathx_1 door_x_1)
               (= pathy_1 door_y_1)
               (= pathdx_1 door_dx_1)))
		;; the door is on the left wall
		( = door_1_left (and
               (= pathx_1 door_x_1)
               (= pathy_1 door_y_1)
               (= pathdy_1 door_dy_1)))
		;; the door is on the right wall
		( = door_1_right (and
               (= (+ pathx_1 pathdx_1) door_x_1)
               (= pathy_1 door_y_1)
               (= pathdy_1 door_dy_1)))
       
	   (xor
		 door_1_bottom door_1_left door_1_right door_1_top
       )

       ;; the second block of the path must fit the second door
       ;; the door is on the top wall
	   ( = door_2_top (and
               (= pathx_2 door_x_2)
               (= (+ pathy_2 pathdy_2) door_y_2)
               (= pathdx_2 door_dx_2)))
		;; the door is on the bottom wall
		( = door_2_bottom (and
               (= pathx_2 door_x_2)
               (= pathy_2 door_y_2)
               (= pathdx_2 door_dx_2)))
		;; the door is on the left wall
		( = door_2_left (and
               (= pathx_2 door_x_2)
               (= pathy_2 door_y_2)
               (= pathdy_2 door_dy_2)))
		;; the door is on the right wall
		( = door_2_right (and
               (= (+ pathx_2 pathdx_2) door_x_2)
               (= pathy_2 door_y_2)
               (= pathdy_2 door_dy_2)))
       
	   (xor
		 door_2_bottom door_2_left door_2_right door_2_top
       )

       ;; path block i must touch path block i+1
       ;; NOTE: due to ordering, it is always the case that x_path_i < x_path_{i+1}
       ;; XXX: we could simplify/factorize the conditions a little; later tho
       
           ;; path block 1 touches path block 2 from below
           ;;    +----+
           ;;    |2   |
           ;;    |    |
           ;;    +----+
           ;; +----+
           ;; |1   |
           ;; |    |
           ;; +----+
         (= touching_1_2_below (and
               (= (+ pathy_1 pathdy_1) pathy_2)
               (>= (+ pathx_1 pathdx_1) pathx_2)
               (<= (+ pathx_1 pathdx_1) (+ pathx_2 pathdx_2))))

           ;; path block 1 touches path block 2 from above
           ;;
           ;; +----+
           ;; |1   |
           ;; |    |
           ;; +----+
           ;;    +----+
           ;;    |2   |
           ;;    |    |
           ;;    +----+
           ( = touching_1_2_above (and
               (= pathy_1 (+ pathy_2 pathdy_2))
               (>= (+ pathx_1 pathdx_1) pathx_2)
               (<= (+ pathx_1 pathdx_1) (+ pathx_2 pathdx_2))))

           ;; path block 1 touches path block 2 from left: two cases
           ;;
           ;; +----+           + . .+
           ;; |1   |           .    .
           ;; |    |+----+     .    .+----+
           ;; +----+|2   |     .    .|2   |
           ;; .    .|    |     +----+|    |
           ;; .    .+----+     |1   |+----+
           ;; .    .           |    |
           ;; + . .+           +----+
           ;;

           ;; block 1 left-above block 2
           (= touching_1_2_leftup (and
               (= (+ pathx_1 pathdx_1) pathx_2)
               (>= (+ pathy_1 pathdy_1) (+ pathy_2 pathdy_2))
               (<= pathy_1 (+ pathy_2 pathdy_2))))

           ;; block 1 left-below block 2
           (= touching_1_2_leftdown (and
               (= (+ pathx_1 pathdx_1) pathx_2)
               (<= pathy_1 pathy_2)
               (>= (+ pathy_1 pathdy_1) pathy_2)))
			   
		(xor
		 touching_1_2_leftup touching_1_2_leftdown touching_1_2_below touching_1_2_above
       )

       ;; constraint: coordinates
       (>= x_1 0) (<= x_1 ROOM)
       (>= y_1 0) (<= y_1 ROOM)
       (= dx_1 TABLE)
       (= dy_1 TABLE)
       (>= x_2 0) (<= x_2 ROOM)
       (>= y_2 0) (<= y_2 ROOM)
       (= dx_2 TABLE)
       (= dy_2 TABLE)
       (>= x_3 0) (<= x_3 ROOM)
       (>= y_3 0) (<= y_3 ROOM)
       (= dx_3 TABLE)
       (= dy_3 TABLE)

       ;; constraint: inside image
       (<= (+ x_1 dx_1) ROOM)
       (<= (+ y_1 dy_1) ROOM)
       (<= (+ x_2 dx_2) ROOM)
       (<= (+ y_2 dy_2) ROOM)
       (<= (+ x_3 dx_3) ROOM)
       (<= (+ y_3 dy_3) ROOM)

       ;; constraint: no overlap
       ;; overlap_1_2
       (or
           (<= (+ x_1 (+ dx_1  ACC)) x_2)
           (<= (+ y_1 (+ dy_1  ACC)) y_2)
           (<= (+ x_2 (+ dx_2  ACC)) x_1)
           (<= (+ y_2 (+ dy_2  ACC)) y_1)
       )
       ;;overlap_1_3
        (or
           (<= (+ x_1 (+ dx_1  ACC)) x_3)
           (<= (+ y_1 (+ dy_1  ACC)) y_3)
           (<= (+ x_3 (+ dx_3  ACC)) x_1)
           (<= (+ y_3 (+ dy_3  ACC)) y_1)
       )
       ;;overlap_2_3
       (or
           (<= (+ x_2 (+ dx_2  ACC)) x_3)
           (<= (+ y_2 (+ dy_2  ACC)) y_3)
           (<= (+ x_3 (+ dx_3  ACC)) x_2)
           (<= (+ y_3 (+ dy_3  ACC)) y_2)
       )
       ;; overlap_1_door1
       (or
           (<= (+ x_1 (+ dx_1  ACC)) door_x_1)
           (<= (+ y_1 (+ dy_1  ACC)) door_y_1)
           (<= (+ door_x_1 (+ door_dx_1  ACC)) x_1)
           (<= (+ door_y_1 (+ door_dy_1  ACC)) y_1)
       )
       ;; overlap_1_door2
       (or
           (<= (+ x_1 (+ dx_1  ACC)) door_x_2)
           (<= (+ y_1 (+ dy_1  ACC)) door_y_2)
           (<= (+ door_x_2 (+ door_dx_2  ACC)) x_1)
           (<= (+ door_y_2 (+ door_dy_2  ACC)) y_1)
       )
       ;; overlap_2_door1
       (or
           (<= (+ x_2 (+ dx_2  ACC)) door_x_1)
           (<= (+ y_2 (+ dy_2  ACC)) door_y_1)
           (<= (+ door_x_1 (+ door_dx_1  ACC)) x_2)
           (<= (+ door_y_1 (+ door_dy_1  ACC)) y_2)
       )
       ;; overlap_2_door2
       (or
           (<= (+ x_2 (+ dx_2  ACC)) door_x_2)
           (<= (+ y_2 (+ dy_2  ACC)) door_y_2)
           (<= (+ door_x_2 (+ door_dx_2  ACC)) x_2)
           (<= (+ door_y_2 (+ door_dy_2  ACC)) y_2)
       )
       ;; overlap_3_door1
       (or
           (<= (+ x_3 (+ dx_3  ACC)) door_x_1)
           (<= (+ y_3 (+ dy_3  ACC)) door_y_1)
           (<= (+ door_x_1 (+ door_dx_1  ACC)) x_3)
           (<= (+ door_y_1 (+ door_dy_1  ACC)) y_3)
       )
       ;; overlap_3_door2
       (or
           (<= (+ x_3 (+ dx_3  ACC)) door_x_2)
           (<= (+ y_3 (+ dy_3  ACC)) door_y_2)
           (<= (+ door_x_2 (+ door_dx_2  ACC)) x_3)
           (<= (+ door_y_2 (+ door_dy_2  ACC)) y_3)
       )

       ;; ordering
       (<= x_1 x_2)
       (<= x_1 x_3)
       (<= x_2 x_3)
       (<= door_x_1 door_x_2)
       (= ordered_1_2 (<= pathx_1 pathx_2))
	   ordered_1_2

       ;; distances _ modulo differenza dell origine
       (ite (> x_1 x_2) (= DX_1_2 (- x_1 x_2)) (= DX_1_2 (- x_2 x_1)))
       (ite (> y_1 y_2) (= DY_1_2 (- y_1 y_2)) (= DY_1_2 (- y_2 y_1)))
       (ite (> x_1 x_3) (= DX_1_3 (- x_1 x_3)) (= DX_1_3 (- x_3 x_1)))
       (ite (> y_1 y_3) (= DY_1_3 (- y_1 y_3)) (= DY_1_3 (- y_3 y_1)))
       (ite (> x_2 x_3) (= DX_2_3 (- x_2 x_3)) (= DX_2_3 (- x_3 x_2)))
       (ite (> y_2 y_3) (= DY_2_3 (- y_2 y_3)) (= DY_2_3 (- y_3 y_2)))

       ;; sum of all distances
       (= sum_DX (+ DX_1_2 DX_1_3 DX_2_3))
       (= sum_DY (+ DY_1_2 DY_1_3 DY_2_3))

       ;; cost function
       ;; minimize path length
       (= cost (+ (* 1 sum_DX) (* 1 sum_DY) (* 1 pathdx_1) (* 1 pathdx_2) (* 1 pathdy_1) (* 1 pathdy_2))
       )
    )
)
(maximize cost)
(check-sat)
(set-model -1)
(get-model)
(exit)
