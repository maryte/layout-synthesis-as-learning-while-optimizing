import Image, ImageDraw, ImageOps
import sys
from collections import defaultdict

def _read_raw_assignments(lines):
    # XXX taken from pylmt/util.py:OptiMathSAT5
    lines = [line for line in lines if not line.startswith("#")]
    if not lines[0] == "sat":
        raise RuntimeError("the problem is unsatisfiable!")
    assignments = {}
    for line in lines[1:]:
        parts = map(str.strip, line.split())
        parts = [part for part in parts if part not in ["(", ")"]]
        if not len(parts):
            continue
        parts[-1] = parts[-1][:-1]
        k, v = parts[0].lstrip("("), " ".join(parts[1:])
        assert sum(c == "(" for c in v) == sum(c == ")" for c in v), \
               "unbalanced parens found in '{}'".format(v)
        assignments[k] = "(= {} {})".format(k, v)
    return assignments

def libsmt2float(s):
    parts = [part.strip("() ") for part in s.split()]
    parts = [part for part in parts if len(part)]
    assert parts[0] == "="
    if len(parts) == 3:
        if parts[2].startswith("true"):
            return 1.0
        elif parts[2].startswith("false"):
            return -1.0
        else:
            return float(parts[2])
    elif len(parts) == 5 and parts[2] == "/":
        # example: '(= FEAT_vertical_material (/ 1 2))'
        return float(parts[3]) / float(parts[4])
    elif len(parts) == 6 and parts[2] == "-" and parts[3] == "/":
        # example: '(= separation (- (/ 462499936509899335 81064793292668928)))'
        return -float(parts[4]) / float(parts[5])
    raise NotImplementedError("unhandled assignment string '{}': '{}'".format(s, parts))

def readBlocks(filename,edge):
    """Reads the x_* and y_* assignments from model files like::

        sat
        ( (cost (/ 72057594037927937 18014398509481984))
          ...
          (x_1 0)
          (y_1 (/ 18014398509481985 36028797018963968))
          (x_2 0)
          (y_2 (/ 18014398509481985 72057594037927936))
          ...
          (proxy_var1_0 true) )
    """
    with open(filename) as fp:
        lines = filter(lambda line: line[0] != "#", map(str.strip, fp.readlines()))
    variable_to_value = _read_raw_assignments(lines)

    data = defaultdict(list)
    for variable, value in sorted(variable_to_value.iteritems()):
        if variable.startswith("x_") or variable.startswith("y_") or \
           variable.startswith("dx_") or variable.startswith("dy_"):
            data[variable.split("_")[0]].append(libsmt2float(value) * edge)
    print data

    x=data["x"]
    y=data["y"]
    dx=data["dx"]
    dy=data["dy"]
    rectangles=[0] * len(x)
    for i in range(len(x)):
        rectangles[i]=[(x[i],y[i]),(x[i]+dx[i],y[i]+dy[i])]
    return rectangles
    

def main():
    colors = { 0 : "yellow", 1 : "orange", 2 : "red"}

    if len(sys.argv) < 4:
        print "Usage: %s <resfile> <image_edge> <dstfile> [<inputfile>]" % sys.argv[0]
        sys.exit (1)

    resfile=sys.argv[1]
    edge=int(sys.argv[2])
    dstfile=sys.argv[3]
    inputfile=""
    if len(sys.argv) > 4:
        inputfile=sys.argv[4]
    
    rectangles=readBlocks(resfile,edge)
    
    im = Image.new("RGB",(edge,edge),'white')

    draw = ImageDraw.Draw(im)

    print("OUTPUT")
    i = 0
    scale = 1.0 / edge
    for rectangle in rectangles:
        draw.rectangle(rectangle,fill=colors[i%3])
        print "rectangle[%d] = (%f, %f, %f, %f)" % (i, rectangle[0][0] * scale, rectangle[0][1] * scale, rectangle[1][0] * scale, rectangle[1][1] * scale)
        i += 1

    if inputfile:
    	i = 1
        rectangles=readBlocks(inputfile,edge)
        print("INPUT")
        for rectangle in rectangles:
            draw.rectangle(rectangle,fill="blue")
            print "rectangle[%d] = (%f, %f, %f, %f)" % (i, rectangle[0][0] * scale, rectangle[0][1] * scale, rectangle[1][0] * scale, rectangle[1][1] * scale)
            i += 1
        
    del draw

    # imageDraw [0,0] is on top left
    im = ImageOps.flip(im)

    # write to stdout
    im.save(dstfile, "PNG")

if __name__ == "__main__":
    main()
