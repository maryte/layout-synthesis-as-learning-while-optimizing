import sys, math, fractions

def number_to_smt2(x):
    z = fractions.Fraction(x)
    p, q = z.numerator, z.denominator
    ret = "(/ {} {})".format(abs(p), q)
    if p < 0:
        ret = "(- {})".format(ret)
    return ret

# VALUES = (0, -0, 1, -1, 1.0, -1.0, 2.0, -2.0, 0.5, -0.5)
# for value in VALUES:
#     print value, "->", number_to_smt2(value)
# sys.exit(1)

def main():

    #####################################################################
    # Input
    #####################################################################

	if len (sys.argv) != 5:
		print "Usage: <num blocks> <configuration={0 office||1 cafe}> <tables={0 single||1 double}> <template={true||false}"
		sys.exit (1)
		
	n=int(sys.argv[1])+1
	conf = int(sys.argv[2])
	tables = bool(sys.argv[3])
	template = bool(sys.argv[4])
	infty=10000
	table = 0.2
	acc = 0.05 # 0.05
	
	#####################################################################
    # Template LMT
    #####################################################################
	
	if template:
		# blocks go from 1 to n
		blocks = range (1, n)
		print ";; blocks =", blocks
			
		print "(set-logic QF_LRA)"
		print "(set-option :produce-models true)"
		   
		#####################################################################
		# Variables
		#####################################################################
			
		print "\n;; variable: cost"
		print "(declare-fun cost () Real)"
		print "\n;; variable: delta"
		print "(declare-fun delta () Real)"		
		print "\n;; variable: separation"
		print "(declare-fun separation () Real)"
		
		print "\n;;parameters"
		print "(declare-fun ROOM() Real) ;;lato stanza"
		print "(declare-fun TABLE() Real) ;;lato blocchetto"
		print "(declare-fun ACC() Real) ;; accessibility"
		
		print "\n;; variable: block coordinate"
		for i in blocks:
			print "(declare-fun x_%d () Real)" %i
			print "(declare-fun y_%d () Real)" %i 

		print "\n;; variable: block dimensions"
		for i in blocks:
			print "(declare-fun dx_%d () Real)" %i
			print "(declare-fun dy_%d () Real)" %i

		print "\n;; distances"
		for i in range (1,n):
			for j in range (i+1,n):
				print "(declare-fun DX_%d_%d () Real)" % (i,j)
				
		for i in range (1,n):
			for j in range (i+1,n):
				print "(declare-fun DY_%d_%d () Real)" % (i,j)

		print "(declare-fun sum_DX () Real)"
		print "(declare-fun sum_DY () Real)"
		
		print "\n;;table composition"
		print "(declare-fun num_1_1_tables () Real)"
		print "(declare-fun num_2_1_tables () Real)"
				

		#####################################################################
		# Problem definition
		#####################################################################


		print "\n;; problem"
		print "(assert"
		print "   (and"

		print "\n       ;; constraint: hack"
		print "       (= ROOM 1)"
		print "       (= TABLE %s)" % number_to_smt2(table)
		print "       (= ACC %s)" % number_to_smt2(acc)

		#####################################################################
		# block coordinate constraints 
		#####################################################################

		print "\n       ;; constraint: coordinates"
		for i in blocks:
			print "       (>= x_%d 0) (<= x_%d ROOM)" % (i,i)
			print "       (>= y_%d 0) (<= y_%d ROOM)" % (i,i)
			print "       (or (= dx_%d TABLE)(= dx_%d (* 2 TABLE)))" % (i,i)
			print "       (= dy_%d TABLE)" % (i)
			
		#####################################################################
		# inside image constraints
		#####################################################################

		print "\n       ;; constraint: inside image"
		for i in blocks:
			print "       (<= (+ x_%d dx_%d) ROOM)" % (i,i)
			print "       (<= (+ y_%d dy_%d) ROOM)" % (i,i)

		#####################################################################
		# no overlap constraints
		#####################################################################

		print "\n       ;; constraint: no overlap"
		for i in blocks:
			for j in range(i+1,n):
				print "       ;;overlap_%d_%d" % (i,j)
				print "       (or"
				print "           (<= (+ x_%d (+ dx_%d  ACC)) x_%d)" % (i,i,j)
				print "           (<= (+ y_%d (+ dy_%d  ACC)) y_%d)" % (i,i,j)
				print "           (<= (+ x_%d (+ dx_%d  ACC)) x_%d)" % (j,j,i)
				print "           (<= (+ y_%d (+ dy_%d  ACC)) y_%d)" % (j,j,i)
				print "       )"
		
		#####################################################################
		# ordering
		#####################################################################


		print "\n       ;; ordering"
		for i in range (1,n):
			for j in range (i+1,n):
				print "       (<= x_%d x_%d)" % (i,j)
				
		#####################################################################
		# distances
		#####################################################################
		
		print "\n       ;; distances"
		for i in range (1,n):
			for j in range (i+1,n):
				print "       (ite (> x_%d x_%d) (= DX_%d_%d (- x_%d x_%d)) (= DX_%d_%d (- x_%d x_%d)))" % (i,j,i,j,i,j,i,j,j,i)
				print "       (ite (> y_%d y_%d) (= DY_%d_%d (- y_%d y_%d)) (= DY_%d_%d (- y_%d y_%d)))" % (i,j,i,j,i,j,i,j,j,i)

		print "\n       ;; sum of all distances"
		pairs = [(i,j) for i in range(1,n) for j in range(i+1,n)]
		if len(pairs) == 1:
			print "       (= sum_DX DX_%d_%d)" % (pairs[0][0], pairs[0][1])
			print "       (= sum_DY DY_%d_%d)" % (pairs[0][0], pairs[0][1])
		else:
			print "       (= sum_DX (+ %s))" % (" ".join("DX_%d_%d" % (i,j) for i,j in pairs))
			print "       (= sum_DY (+ %s))" % (" ".join("DY_%d_%d" % (i,j) for i,j in pairs))

		#####################################################################
		# tables' shapes
		#####################################################################
		
		
		print "\n       ;; tables' shapes"
		print "       (= num_1_1_tables (+"
		for i in range (1,n):
			print "              (ite (<= dx_%d TABLE) 1 0)" % (i)
		print "       ))"
		print "       (= num_2_1_tables (+"
		for i in range (1,n):
			print "              (ite (> dx_%d TABLE) 1 0)" % (i)
		print "       ))"
				
		

		
		print "\n       ;; cost function"
		
		if conf:
			print "       (= cost (+ (* 1 sum_DX) (* 1 sum_DY) (* 1 num_1_1_tables) (* 2 num_2_1_tables))"
		else:
			print "       (= cost (+ (* (- 0 1) sum_DX) (* (- 0 1) sum_DY) (* 2 num_1_1_tables) (* 1 num_2_1_tables))"

		print "       )"
		print "	)" 
		print ")"
		print "(minimize cost)"
		print "(check-sat)"
		print "(set-model -1)"
		print "(get-model)"
		print "(exit)"
		
		
	#####################################################################
    # Inference problem SMT2
    #####################################################################	
		
	else:
		# blocks go from 1 to n
		blocks = range (1, n)
		print ";; blocks =", blocks
			
		print "(set-logic QF_LRA)"
		   
		#####################################################################
		# Variables
		#####################################################################
			
		print "\n;; variable: cost"
		print "(declare-fun cost () Real)"
		print "(declare-fun cost_ () Real)"
		print "\n;; variable: delta"
		print "(declare-fun delta () Real)"
		print "(declare-fun delta_ () Real)"
		
		print "\n;; variable: separation"
		print "(declare-fun separation () Real)"
		print "(declare-fun separation_ () Real)"
		
		print "\n;;parameters"
		print "(declare-fun ROOM() Real) ;;lato stanza"
		print "(declare-fun TABLE() Real) ;;lato blocchetto"
		print "(declare-fun ACC() Real) ;; accessibility"
		
		print "\n;; variable: block coordinate"
		for i in blocks:
			print "(declare-fun x_%d () Real)" %i
			print "(declare-fun y_%d () Real)" %i 

		print "\n;; variable: block dimensions"
		for i in blocks:
			print "(declare-fun dx_%d () Real)" %i
			print "(declare-fun dy_%d () Real)" %i

		print "\n;; distances"
		for i in range (1,n):
			for j in range (i+1,n):
				print "(declare-fun DX_%d_%d () Real)" % (i,j)
				
		for i in range (1,n):
			for j in range (i+1,n):
				print "(declare-fun DY_%d_%d () Real)" % (i,j)

		print "(declare-fun sum_DX () Real)"
		print "(declare-fun sum_DY () Real)"
				
		print "\n;; overlap"
		for i in range (1,n):
			for j in range (i+1,n):
				print "(declare-fun overlap_%d_%d () Bool)" % (i,j)

		print "\n;; dx_touching_i_j for each j>i"
		for i in range (1,n):
			for j in range (i+1,n):
				print "(declare-fun dx_touching_%d_%d () Bool)" % (i,j)

		print ";; over_touching_i_j for each j>i"
		for i in range (1,n):
			for j in range (i+1,n):
				print "(declare-fun over_touching_%d_%d () Bool)" % (i,j)

		#####################################################################
		# Problem definition
		#####################################################################


		print "\n;; problem"
		print "(assert"
		print "   (and"

		print "\n       ;; constraint: hack"
		print "       (= cost cost_)"
		# print "       (< cost %d)" %infty # XXX just an optimization, pointless for the moment
		print "       (= delta delta_)"
		print "       (= separation separation_)"
		print "       (= ROOM 1)"
		print "       (= TABLE %s)" % number_to_smt2(table)
		print "       (= ACC %s)" % number_to_smt2(acc)

		#####################################################################
		# block coordinate constraints 
		#####################################################################

		print "\n       ;; constraint: coordinates"
		for i in blocks:
			print "       (>= x_%d 0) (<= x_%d ROOM)" % (i,i)
			print "       (>= y_%d 0) (<= y_%d ROOM)" % (i,i)
			print "       (= dx_%d TABLE)" % (i)
			print "       (= dy_%d TABLE)" % (i)
			
		#####################################################################
		# inside image constraints
		#####################################################################

		print "\n       ;; constraint: inside image"
		for i in blocks:
			print "       (<= (+ x_%d dx_%d) ROOM)" % (i,i)
			print "       (<= (+ y_%d dy_%d) ROOM)" % (i,i)

		#####################################################################
		# no overlap constraints
		#####################################################################

		print "\n       ;; constraint: no overlap"
		for i in blocks:
			for j in range(i+1,n):
				print "       ;;%d-%d" % (i,j)
				print "       (= overlap_%d_%d (or " % (i,j)
				print "           (<= (+ x_%d (+ dx_%d  ACC)) x_%d)" % (i,i,j)
				print "           (<= (+ y_%d (+ dy_%d  ACC)) y_%d)" % (i,i,j)
				print "           (<= (+ x_%d (+ dx_%d  ACC)) x_%d)" % (j,j,i)
				print "           (<= (+ y_%d (+ dy_%d  ACC)) y_%d)" % (j,j,i)
				print "       ))"

		for i in range (1,n):
			for j in range (i+1,n):
				print "       overlap_%d_%d " % (i,j)
			
		#####################################################################
		# ordering
		#####################################################################


		print "\n       ;; ordering"
		for i in range (1,n):
			for j in range (i+1,n):
	#            print "       (<= x_%d x_%d) (<= y_%d y_%d)" % (i,j,i,j) # XXX troppo vincolante
				print "       (<= x_%d x_%d)" % (i,j)
				
		#####################################################################
		# distances
		#####################################################################
		
		print "\n       ;; distances _ modulo differenza dell origine"
		for i in range (1,n):
			for j in range (i+1,n):
				print "       (ite (> x_%d x_%d) (= DX_%d_%d (- x_%d x_%d)) (= DX_%d_%d (- x_%d x_%d)))" % (i,j,i,j,i,j,i,j,j,i)
				print "       (ite (> y_%d y_%d) (= DY_%d_%d (- y_%d y_%d)) (= DY_%d_%d (- y_%d y_%d)))" % (i,j,i,j,i,j,i,j,j,i)

		print "\n       ;; sum of all distances"
		pairs = [(i,j) for i in range(1,n) for j in range(i+1,n)]
		if len(pairs) == 1:
			print "       (= sum_DX DX_%d_%d)" % (pairs[0][0], pairs[0][1])
			print "       (= sum_DY DY_%d_%d)" % (pairs[0][0], pairs[0][1])
		else:
			print "       (= sum_DX (+ %s))" % (" ".join("DX_%d_%d" % (i,j) for i,j in pairs))
			print "       (= sum_DY (+ %s))" % (" ".join("DY_%d_%d" % (i,j) for i,j in pairs))

		#####################################################################
		# touching
		#####################################################################
		
		
		print "\n       ;; dx_touching constraints"
		for i in range (1,n):
			for j in range (i+1,n):
				print "       (= dx_touching_%d_%d (and" % (i,j)
				print "              (>= (+ x_%d dx_%d) x_%d)" % (i,i,j)
				print "              (<= (+ x_%d dx_%d) x_%d)" % (i,i,j)
				print "              (>= y_%d y_%d)" % (i,j)
				print "              (<= y_%d y_%d)" % (i,j)
				print "       ))"
				
		print "\n       ;; over_touching constraints"
		for i in range (1,n):
			for j in range (i+1,n):
				print "       (= over_touching_%d_%d (and" % (i,j)
				print "              (>= (+ y_%d dy_%d) y_%d)" % (i,i,j)
				print "              (<= (+ y_%d dy_%d) y_%d)" % (i,i,j)
				print "              (>= x_%d x_%d)" % (i,j)
				print "              (<= x_%d x_%d)" % (i,j)    
				print "       ))"

		
		print "\n       ;; cost function"
		
		if conf:
			print "       (= cost (+ (* 1 sum_DX) (* 1 sum_DY)))"
		else:
			print "       (= cost (+ (* (- 0 1) sum_DX) (* (- 0 1) sum_DY)))"

		print "       ))"
		print "))" 
		print "(check-sat)"
		print "(exit)"
		

if __name__ == "__main__":
    main()
