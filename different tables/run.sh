#!/bin/bash

OMT="../../Code/optimathsat/optimathsat"
mkdir -p outputs

# python make_smt2.py 3 0 0 true > test.smt2
# $OMT layout_different_tables.smt2  > out.model
# $OMT test.smt2  > out.model
# python draw_result.py out.model 512 out.png

for type in 0 1
do
    for num_blocks in `seq 2 4`
    do
		if [ "x$type" == "x0" ]; then 
			conf="office"
		else # layout>0
			conf="cafe"
		fi
        basename=outputs/layout_${conf}_${num_blocks}
        echo "running for $basename"
        python make_smt2.py $num_blocks $type 0 true > $basename.smt2 #true=template, false=inference problem
        $OMT $basename.smt2 > $basename.model
        python draw_result.py $basename.model 512 $basename.png
    done
done
