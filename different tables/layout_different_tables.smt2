;; blocks = [1, 2, 3]
(set-logic QF_LRA)
(set-option :produce-models true)

;; variable: cost
(declare-fun cost () Real)

;; variable: delta
(declare-fun delta () Real)

;; variable: separation
(declare-fun separation () Real)

;;parameters
(declare-fun ROOM() Real) ;;lato stanza
(declare-fun TABLE() Real) ;;lato blocchetto
(declare-fun ACC() Real) ;; accessibility

;; variable: block coordinate
(declare-fun x_1 () Real)
(declare-fun y_1 () Real)
(declare-fun x_2 () Real)
(declare-fun y_2 () Real)
(declare-fun x_3 () Real)
(declare-fun y_3 () Real)

;; variable: block dimensions
(declare-fun dx_1 () Real)
(declare-fun dy_1 () Real)
(declare-fun dx_2 () Real)
(declare-fun dy_2 () Real)
(declare-fun dx_3 () Real)
(declare-fun dy_3 () Real)

;; distances
(declare-fun DX_1_2 () Real)
(declare-fun DX_1_3 () Real)
(declare-fun DX_2_3 () Real)
(declare-fun DY_1_2 () Real)
(declare-fun DY_1_3 () Real)
(declare-fun DY_2_3 () Real)
(declare-fun sum_DX () Real)
(declare-fun sum_DY () Real)

;; table composition
(declare-fun num_1_1_tables () Real)
(declare-fun num_2_1_tables () Real)

;; problem
(assert
   (and

       ;; constraint: hack
       (= ROOM 1)
       (= TABLE (/ 3602879701896397 18014398509481984))
       (= ACC (/ 3602879701896397 72057594037927936))

       ;; constraint: coordinates
       (>= x_1 0) (<= x_1 ROOM)
       (>= y_1 0) (<= y_1 ROOM)
       (or (= dx_1 TABLE)(= dx_1 (* 2 TABLE)))
	   (= dy_1 TABLE)
       (>= x_2 0) (<= x_2 ROOM)
       (>= y_2 0) (<= y_2 ROOM)
       (or (= dx_2 TABLE)(= dx_2 (* 2 TABLE)))
       (= dy_2 TABLE)
	   (>= x_3 0) (<= x_3 ROOM)
       (>= y_3 0) (<= y_3 ROOM)
       (or (= dx_3 TABLE)(= dx_3 (* 2 TABLE)))
       (= dy_3 TABLE)

       ;; constraint: inside image
       (<= (+ x_1 dx_1) ROOM)
       (<= (+ y_1 dy_1) ROOM)
       (<= (+ x_2 dx_2) ROOM)
       (<= (+ y_2 dy_2) ROOM)
       (<= (+ x_3 dx_3) ROOM)
       (<= (+ y_3 dy_3) ROOM)

       ;; constraint: no overlap
       ;; overlap_1_2
       (or 
           (<= (+ x_1 (+ dx_1  ACC)) x_2)
           (<= (+ y_1 (+ dy_1  ACC)) y_2)
           (<= (+ x_2 (+ dx_2  ACC)) x_1)
           (<= (+ y_2 (+ dy_2  ACC)) y_1)
       )
       ;;overlap_1_3
        (or 
           (<= (+ x_1 (+ dx_1  ACC)) x_3)
           (<= (+ y_1 (+ dy_1  ACC)) y_3)
           (<= (+ x_3 (+ dx_3  ACC)) x_1)
           (<= (+ y_3 (+ dy_3  ACC)) y_1)
       )
       ;;overlap_2_3
       (or 
           (<= (+ x_2 (+ dx_2  ACC)) x_3)
           (<= (+ y_2 (+ dy_2  ACC)) y_3)
           (<= (+ x_3 (+ dx_3  ACC)) x_2)
           (<= (+ y_3 (+ dy_3  ACC)) y_2)
       )

       ;; ordering
       (<= x_1 x_2)
       (<= x_1 x_3)
       (<= x_2 x_3)

       ;; distances
       (ite (> x_1 x_2) (= DX_1_2 (- x_1 x_2)) (= DX_1_2 (- x_2 x_1)))
       (ite (> y_1 y_2) (= DY_1_2 (- y_1 y_2)) (= DY_1_2 (- y_2 y_1)))
       (ite (> x_1 x_3) (= DX_1_3 (- x_1 x_3)) (= DX_1_3 (- x_3 x_1)))
       (ite (> y_1 y_3) (= DY_1_3 (- y_1 y_3)) (= DY_1_3 (- y_3 y_1)))
       (ite (> x_2 x_3) (= DX_2_3 (- x_2 x_3)) (= DX_2_3 (- x_3 x_2)))
       (ite (> y_2 y_3) (= DY_2_3 (- y_2 y_3)) (= DY_2_3 (- y_3 y_2)))

       ;; sum of all distances
       (= sum_DX (+ DX_1_2 DX_1_3 DX_2_3))
       (= sum_DY (+ DY_1_2 DY_1_3 DY_2_3))
    
	   ;; num tables 
	    (= num_1_1_tables (+                                                                                                                                                                                                                       
			(ite (<= dx_1 TABLE) 1 0)                                               
			(ite (<= dx_2 TABLE) 1 0)                                               
			(ite (<= dx_3 TABLE) 1 0)))  
			
		(= num_2_1_tables (+                                                                                                                                                                                                                       
			(ite (> dx_1 TABLE) 1 0)                                               
			(ite (> dx_2 TABLE) 1 0)                                               
			(ite (> dx_3 TABLE) 1 0))) 
		
       ;; cost function
       (= cost (+ (* (- 0 1) sum_DX) (* (- 0 1) sum_DY) (* 2 num_1_1_tables) (* 1 num_2_1_tables))
	   ;;(= cost (+ (* 1 sum_DX) (* 1 sum_DY) (* 2 num_1_1_tables) (* 1 num_2_1_tables))
	   )
	)
)
(minimize cost)
(check-sat)
(set-model -1)
(get-model)
(exit)
