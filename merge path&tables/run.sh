#!/bin/bash

OMT="../../Code/optimathsat/optimathsat"
mkdir -p outputs

declare -a arr=("lu" "ld" "lr" "dr" "ur")

#for room in 1 2
for room in 2
	do
	for type in 0 1
	do
		for doors in "${arr[@]}"
		do 
			for num_blocks in `seq 2 6`
			do
				if [ "x$type" == "x0" ]; then 
					conf="office"
				else # layout>0
					conf="cafe"
				fi
				basename=outputs/layout_${conf}_${num_blocks}_${doors}_${room}
				echo "running for $basename"
				#Usage: <num blocks> <configuration={0 office||1 cafe}> <doors positions={lu||ld||lr||dr||ur}> <room size> <template={true||false}
				python make_smt2.py $num_blocks $type $doors $room true > $basename.smt2 #true=template, false=inference problem
				$OMT $basename.smt2 > $basename.model
				python draw_result.py $basename.model 512 $basename.png
			done
		done
	done
done
