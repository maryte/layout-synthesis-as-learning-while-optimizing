import sys, math, fractions

def number_to_smt2(x):
	if x!=0:
		z = fractions.Fraction(x)
		p, q = z.numerator, z.denominator
		ret = "(/ {} {})".format(abs(p), q)
		if p < 0:
			ret = "(- {})".format(ret)
		return ret
	else:
		return 0

# VALUES = (0, -0, 1, -1, 1.0, -1.0, 2.0, -2.0, 0.5, -0.5)
# for value in VALUES:
#     print value, "->", number_to_smt2(value)
# sys.exit(1)

def cafe(n,dmax,table,acc):

    print "%s cafe generation" %n
    punti=[0]*(n+1)
	
    x_ = 0.0
    y_ = 0.0
    punti[1]=["(= x_1 %d) " %(x_)]
    punti[1].append("(= y_1 %d) " %(y_))

    for i in range(2,n+1):
		x_ = x_ + table + acc
		if x_ > (dmax-table):
			x_ = 0.0
			y_ = y_ + table + acc
		punti[i]= ["(= x_%d %s) " %(i,number_to_smt2(x_))]
		punti[i].append("(= y_%d %s) " %(i,number_to_smt2(y_)))     

    for i in range(1,n+1):
        punti[i].append("(= dx_%d %s) " %(i,number_to_smt2(table)))

    for i in range(1,n+1):
        punti[i].append("(= dy_%d %s) " %(i,number_to_smt2(table)))

    return punti


def office(n,dmax,table,acc):

    punti=[0]*(n+1)

    punti[1]=["(= x_1 0) "]
    for i in range(2,n+1):
        punti[i]=["(= x_%d (/ %d %d)) " %(i,i-1,n)]

    punti[1].append("(= y_1 0) ")
    for i in range(2,n+1):
        punti[i].append("(= y_%d (/ %d %d)) " %(i,i-1,n))

    for i in range(1,n+1):
        punti[i].append("(= dx_%d (/ 1 %d)) " %(i,n))

    for i in range(1,n+1):
        punti[i].append("(= dy_%d (/ 1 %d)) " %(i,n))

    return punti

def print_list(fp,n,points):

    fp.write (" ".join(points[1]) + " ; " + " ".join([" ".join(point) for point in points[2:]]) + "\n")

def main():

    if len (sys.argv) != 4:
        print "Usage: %s <minblocks> <maxblocks> <numtestblocks>" % sys.argv[0]
        sys.exit (1)

    minblocks = int (sys.argv[1])
    maxblocks = int (sys.argv[2])
    numtestblocks = int (sys.argv[3])

    dmax = 1.0
    table = 0.2
    acc = 0.05


    configs = (
        ("inputs/input-cafe.lmt", cafe),
        ("inputs/input-office.lmt", office)
    )

    for config in configs:
        fp = open (config[0], "w")
        fp.write ("1\n")
        for n in range (minblocks, maxblocks):
            points = config[1] (n, dmax, table, acc)
            fp.write ("template-%d.smt2\n" %  n)
            print_list (fp, n, points)
        fp.write ("template-%d.smt2\n" % numtestblocks)
        fp.close ()

if __name__ == "__main__":
    main()
