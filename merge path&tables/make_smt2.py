import sys, math, fractions

def number_to_smt2(x):
    z = fractions.Fraction(x)
    p, q = z.numerator, z.denominator
    ret = "(/ {} {})".format(abs(p), q)
    if p < 0:
        ret = "(- {})".format(ret)
    return ret

# VALUES = (0, -0, 1, -1, 1.0, -1.0, 2.0, -2.0, 0.5, -0.5)
# for value in VALUES:
#     print value, "->", number_to_smt2(value)
# sys.exit(1)

def main():

    #####################################################################
    # Input
    #####################################################################

	if len (sys.argv) != 6:
		print ";;Usage: <num blocks> <configuration={0 office||1 cafe}> <doors positions={lu||ld||lr||dr||ur}> <room size> <template={true||false}"
		sys.exit (1)
		
	n=int(sys.argv[1])+1
	conf = int(sys.argv[2])
	doors = (sys.argv[3])
	room = int(sys.argv[4])
	template = bool(sys.argv[5])

	
	if ((doors != "lu") and (doors != "ld") and (doors != "lr") and (doors != "dr") and (doors != "ur")):
		print ";;Incorrect doors configuration"
		sys.exit (1)
	#else: print ";;Correct!"
		
	infty=10000
	table = 0.2
	door = 0.2
	acc = 0.05 # 0.05
	
	#####################################################################
    # Template LMT
    #####################################################################
	
	if template:
		# blocks go from 1 to n
		blocks = range (1, n)
		print ";; blocks =", blocks
			
		print "(set-logic QF_LRA)"
		print "(set-option :produce-models true)"
		   
		#####################################################################
		# Variables
		#####################################################################
			
		print "\n;; variable: cost"
		print "(declare-fun cost () Real)"
		print "\n;; variable: delta"
		print "(declare-fun delta () Real)"		
		print "\n;; variable: separation"
		print "(declare-fun separation () Real)"
		
		print "\n;;parameters"
		print "(declare-fun ROOM() Real) ;;lato stanza"
		print "(declare-fun TABLE() Real) ;;lato blocchetto"
		print "(declare-fun DOORSIDE() Real) ;;dimensione porta"
		print "(declare-fun ACC() Real) ;; accessibility"
		
		print "\n;; TABLES"
		print ";; variable: block coordinate"
		for i in blocks:
			print "(declare-fun x_%d () Real)" %i
			print "(declare-fun y_%d () Real)" %i 
		print "\n;; variable: block dimensions"
		for i in blocks:
			print "(declare-fun dx_%d () Real)" %i
			print "(declare-fun dy_%d () Real)" %i
			
		print "\n;; DOORS"
		print ";; variable: door coordinate"
		print "(declare-fun door_x_1 () Real)"
		print "(declare-fun door_y_1 () Real)" 
		print "(declare-fun door_x_2 () Real)"
		print "(declare-fun door_y_2 () Real)" 
		print "\n;; variable: door dimensions"
		print "(declare-fun door_dx_1 () Real)"
		print "(declare-fun door_dy_1 () Real)" 
		print "(declare-fun door_dx_2 () Real)"
		print "(declare-fun door_dy_2 () Real)" 
		
		print "\n;; PATH"
		print ";; variable: door coordinate"
		print "(declare-fun pathx_1 () Real)"
		print "(declare-fun pathy_1 () Real)" 
		print "(declare-fun pathx_2 () Real)"
		print "(declare-fun pathy_2 () Real)" 
		print "\n;; variable: door dimensions"
		print "(declare-fun pathdx_1 () Real)"
		print "(declare-fun pathdy_1 () Real)" 
		print "(declare-fun pathdx_2 () Real)"
		print "(declare-fun pathdy_2 () Real)" 

		print "\n;; distances"
		for i in range (1,n):
			for j in range (i+1,n):
				print "(declare-fun DX_%d_%d () Real)" % (i,j)				
		for i in range (1,n):
			for j in range (i+1,n):
				print "(declare-fun DY_%d_%d () Real)" % (i,j)
		print "(declare-fun sum_DX () Real)"
		print "(declare-fun sum_DY () Real)"
		print "\n;;table composition"
		print "(declare-fun num_1_1_tables () Real)"
		print "(declare-fun num_2_1_tables () Real)"		
		print "\n;;DEBUG"
		print ";;to which door the path block is fitting"
		print "(declare-fun door_1_up () Bool)"
		print "(declare-fun door_1_right () Bool)"
		print "(declare-fun door_1_left () Bool)"
		print "(declare-fun door_1_down () Bool)"
		print "(declare-fun door_2_up () Bool)"
		print "(declare-fun door_2_right () Bool)"
		print "(declare-fun door_2_left () Bool)"
		print "(declare-fun door_2_down () Bool)"
		print ";;path touching way"
		print "(declare-fun touching_1_2_above () Bool)"
		print "(declare-fun touching_1_2_below () Bool)"
		print "(declare-fun touching_1_2_leftabove () Bool)"
		print "(declare-fun touching_1_2_leftbelow () Bool)"
		print ";;ordering"
		print "(declare-fun ordered_1_2 () Bool)"
				
		#####################################################################
		# Problem definition
		#####################################################################

		print "\n;; problem"
		print "(assert"
		print "   (and"

		print "\n       ;; constraint: hack"
		print "       (= ROOM %s)" % number_to_smt2(room)
		print "       (= TABLE %s)" % number_to_smt2(table)
		print "       (= DOORSIDE %s)" % number_to_smt2(table)
		print "       (= ACC %s)" % number_to_smt2(acc)
		
		#####################################################################
		# fixed doors coordinate constraints 
		# doors positions= lu||ld||lr||dr||ur
		#####################################################################

		print "\n       ;; constraint: fixed doors"
		print "       ;; dimensions"
			
		if doors == "lu":
			print "       (= door_dx_1 0)"
			print "       (= door_dy_1 DOORSIDE)"
			print "       (= door_dx_2 DOORSIDE)"
			print "       (= door_dy_2 0)"
			print "       ;;coordinates"
			print "       (= door_x_1 (* 0 ROOM))"
			print "       (= door_y_1 (* (/ 1 3) ROOM))"      
			print "       (= door_x_2 (* (/ 1 3) ROOM))"
			print "       (= door_y_2 (* 1 ROOM))"
		if doors == "ld":
			print "       (= door_dx_1 0)"
			print "       (= door_dy_1 DOORSIDE)"
			print "       (= door_dx_2 DOORSIDE)"
			print "       (= door_dy_2 0)"
			print "       ;;coordinates"
			print "       	(= door_x_1 0)"
			print "       (= door_y_1 (* (/ 1 3) ROOM))"      
			print "       (= door_x_2 (* (/ 1 3) ROOM))" 
			print "       (= door_y_2 0)"
		if doors == "lr":
			print "       ;; dimensions"
			print "       (= door_dx_1 0)"
			print "       (= door_dy_1 DOORSIDE)"
			print "       (= door_dx_2 0)"
			print "       (= door_dy_2 DOORSIDE)"
			print "       ;;coordinates"
			print "       (= door_x_1 0)"
			print "       (= door_y_1 (* (/ 1 3) ROOM))"      
			print "       (= door_x_2 (* 1 ROOM))"
			print "       (= door_y_2 (* (/ 1 3) ROOM))"
		if doors == "dr":
			print "       (= door_dx_1 DOORSIDE)"       
			print "       (= door_dy_1 0)"       
			print "       (= door_dx_2 0)"       
			print "       (= door_dy_2 DOORSIDE)"       
			print "       ;;coordinates"       
			print "       (= door_x_1 (* (/ 1 3) ROOM))"       
			print "       (= door_y_1 0)  "           
			print "       (= door_x_2 (* 1 ROOM))"       
			print "       (= door_y_2 (* (/ 1 3) ROOM))"       
		if doors == "ur":
		   print "       (= door_dx_1 DOORSIDE)"       
		   print "       (= door_dy_1 0)"       
		   print "       (= door_dx_2 0)"       
		   print "       (= door_dy_2 DOORSIDE)"       
		   print "       ;;coordinates"       
		   print "       (= door_x_1 (* (/ 1 3) ROOM))"        
		   print "       (= door_y_1 (* 1 ROOM))    "         
		   print "       (= door_x_2 (* 1 ROOM))"       
		   print "       (= door_y_2 (* (/ 1 3) ROOM))"       
		   
		print "       ;; bounding box"       
		print "       (>= pathx_1 0) (<= pathx_1 ROOM) (>= pathdx_1 0) (<= pathdx_1 ROOM)"       
		print "       (>= pathy_1 0) (<= pathy_1 ROOM) (>= pathdy_1 0) (<= pathdy_1 ROOM)"       
		print "       (>= pathx_2 0) (<= pathx_2 ROOM) (>= pathdx_2 0) (<= pathdx_2 ROOM)"       
		print "       (>= pathy_2 0) (<= pathy_2 ROOM) (>= pathdy_2 0) (<= pathdy_2 ROOM)"       

		
		
		#####################################################################
		# contraints: side of first block fit one door, side of the second block must fit the other door
		# the first block of the path must fit the first door
		# the door is on the up wall
		#####################################################################

		print "\n       ;; contraints: side of first block fit one door, side of the second block must fit the other door"       
		print "       ( = door_1_up (and"       
		print "              (= pathx_1 door_x_1)"       
		print "              (= (+ pathy_1 pathdy_1) door_y_1)"       
		print "              (= pathdx_1 door_dx_1)))"       
		print "       ;; the door is on the down wall"       
		print "       ( = door_1_down (and"       
		print "              (= pathx_1 door_x_1)"       
		print "              (= pathy_1 door_y_1)"       
		print "              (= pathdx_1 door_dx_1)))"       
		print "       ;; the door is on the left wall"       
		print "       ( = door_1_left (and"       
		print "              (= pathx_1 door_x_1)"       
		print "              (= pathy_1 door_y_1)"       
		print "              (= pathdy_1 door_dy_1)))"       
		print "       ;; the door is on the right wall"       
		print "       ( = door_1_right (and"       
		print "              (= (+ pathx_1 pathdx_1) door_x_1)"       
		print "              (= pathy_1 door_y_1)"       
		print "              (= pathdy_1 door_dy_1)))     " 
		print "\n"       		
		print "       (or door_1_down door_1_left door_1_right door_1_up)"       
		print "       (=> door_1_down (not (or door_1_left door_1_right door_1_up)))"       
		print "       (=> door_1_left (not (or door_1_down door_1_right door_1_up)))" 
		print "       (=> door_1_right (not (or door_1_left door_1_down door_1_up)))"
		print "       (=> door_1_up (not (or door_1_left door_1_right door_1_down)))"  
		print "\n"       		
		print "       ;; the second block of the path must fit the second door"       
		print "       ;; the door is on the up wall"       
		print "       ( = door_2_up (and"       
		print "              (= pathx_2 door_x_2)"       
		print "              (= (+ pathy_2 pathdy_2) door_y_2)"       
		print "              (= pathdx_2 door_dx_2)))"       
		print "       ;; the door is on the down wall"       
		print "       ( = door_2_down (and"       
		print "             (= pathx_2 door_x_2)"       
		print "              (= pathy_2 door_y_2)"       
		print "              (= pathdx_2 door_dx_2)))"       
		print "       ;; the door is on the left wall"       
		print "       ( = door_2_left (and"       
		print "              (= pathx_2 door_x_2)"       
		print "              (= pathy_2 door_y_2)"       
		print "              (= pathdy_2 door_dy_2)))"       
		print "       ;; the door is on the right wall"       
		print "       ( = door_2_right (and"       
		print "              (= (+ pathx_2 pathdx_2) door_x_2)"       
		print "             (= pathy_2 door_y_2)"       
		print "             (= pathdy_2 door_dy_2)))"       
		print "\n"       
		print "       (or door_2_down door_2_left door_2_right door_2_up)"       
		print "       (=> door_2_down (not (or door_2_left door_2_right door_2_up)))"       
		print "       (=> door_2_left (not (or door_2_down door_2_right door_2_up)))" 
		print "       (=> door_2_right (not (or door_2_left door_2_down door_2_up)))"   
		print "       (=> door_2_up (not (or door_2_left door_2_right door_2_down)))"       
		
		

		#####################################################################
		# path block i must touch path block i+1
		# NOTE: due to ordering, it is always the case that x_path_i < x_path_{i+1}
		# XXX: we could simplify/factorize the conditions a little; later tho
		#####################################################################

		#####################################################################
		# ;; path block 1 touches path block 2 from below
		# ;;    +----+
		# ;;    |2   |
		# ;;    |    |
		# ;;    +----+
		# ;; +----+
		# ;; |1   |
		# ;; |    |
		# ;; +----+
		#####################################################################

		print "\n       ;; contraints: touching"       
		print "        (= touching_1_2_below (and"       
		print "              (= (+ pathy_1 pathdy_1) pathy_2)"       
		print "              (= (+ pathx_1 pathdx_1) (+ pathx_2 pathdx_2))))"       

		print "        ;; path block 1 touches path block 2 from above"       
		# ;;
		# ;; +----+
		# ;; |1   |
		# ;; |    |
		# ;; +----+
		# ;;    +----+
		# ;;    |2   |
		# ;;    |    |
		# ;;    +----+

		print "       ( = touching_1_2_above (and"       
		print "              (= pathy_1 (+ pathy_2 pathdy_2))"       
		print "              (= (+ pathx_1 pathdx_1) (+ pathx_2 pathdx_2))))"       

		print "       ;; path block 1 touches path block 2 from left: two cases"       

		# ;;
		# ;; +----+           + . .+
		# ;; |1   |           .    .
		# ;; |    |+----+     .    .+----+
		# ;; +----+|2   |     .    .|2   |
		# ;; .    .|    |     +----+|    |
		# ;; .    .+----+     |1   |+----+
		# ;; .    .           |    |
		# ;; + . .+           +----+
		# ;;

		print "       ;; block 1 left-above block 2"       
		print "          (= touching_1_2_leftabove (and"       
		print "              (= (+ pathx_1 pathdx_1) pathx_2)"       
		print "              (= pathy_1 pathy_2)))"       

		print "          ;; block 1 left-below block 2"       
		print "          (= touching_1_2_leftbelow (and"       
		print "              (= (+ pathx_1 pathdx_1) pathx_2)"       
		print "              (= ( + pathy_1 pathdy_1 ) (+ pathy_2 pathdy_2))))\n"       
			   
		print "       (or touching_1_2_leftabove touching_1_2_leftbelow touching_1_2_below touching_1_2_above)"       
		print "       (=> touching_1_2_leftabove (not (or touching_1_2_leftbelow touching_1_2_below touching_1_2_above)))"   
		print "    	  (=> touching_1_2_leftbelow (not (or touching_1_2_leftabove touching_1_2_below touching_1_2_above)))"       
		print "    	  (=> touching_1_2_below (not (or touching_1_2_leftbelow touching_1_2_leftabove touching_1_2_above)))"       
		print "       (=> touching_1_2_above (not (or touching_1_2_leftbelow touching_1_2_below touching_1_2_leftabove)))" 

		#print "       (or touching_1_2_leftabove touching_1_2_leftbelow touching_1_2_below touching_1_2_above)"       
		#print "       (=> touching_1_2_leftabove (not (or touching_1_2_leftbelow touching_1_2_below)))"   
		#print "       (=> touching_1_2_leftbelow (not (or touching_1_2_below touching_1_2_above)))"       
		#print "       (=> touching_1_2_below (not (or touching_1_2_leftbelow touching_1_2_above)))"       
		#print "       (=> touching_1_2_above (not (or touching_1_2_leftbelow touching_1_2_below)))" 
		
		#####################################################################
		# block coordinate constraints 
		#####################################################################

		print "\n       ;; constraint: coordinates"
		for i in blocks:
			print "       (>= x_%d 0) (<= x_%d ROOM)" % (i,i)
			print "       (>= y_%d 0) (<= y_%d ROOM)" % (i,i)
			print "       (or (= dx_%d TABLE)(= dx_%d (* 2 TABLE)))" % (i,i)
			print "       (= dy_%d TABLE)" % (i)
			
		#####################################################################
		# inside image constraints
		#####################################################################

		print "\n       ;; constraint: inside image"
		for i in blocks:
			print "       (<= (+ x_%d dx_%d) ROOM)" % (i,i)
			print "       (<= (+ y_%d dy_%d) ROOM)" % (i,i)

		#####################################################################
		# no overlap constraints
		#####################################################################

		print "\n       ;; constraint: no overlap"
		for i in blocks:
			for j in range(i+1,n):
				print "       ;;overlap_%d_%d" % (i,j)
				print "       (or"
				print "           (<= (+ x_%d (+ dx_%d  ACC)) x_%d)" % (i,i,j)
				print "           (<= (+ y_%d (+ dy_%d  ACC)) y_%d)" % (i,i,j)
				print "           (<= (+ x_%d (+ dx_%d  ACC)) x_%d)" % (j,j,i)
				print "           (<= (+ y_%d (+ dy_%d  ACC)) y_%d)" % (j,j,i)
				print "       )"
		
		print "\n       ;; constraint: no doors overlap"
		for i in blocks:
			for j in range(1, 3):
				print "       ;;overlap_%d_path%d" % (i,j)
				print "       (or"
				print "           (<= (+ x_%d (+ dx_%d  ACC)) pathx_%d)" % (i,i,j)
				print "           (<= (+ y_%d (+ dy_%d  ACC)) pathy_%d)" % (i,i,j)
				print "           (<= (+ pathx_%d (+ pathdx_%d  ACC)) x_%d)" % (j,j,i)
				print "           (<= (+ pathy_%d (+ pathdy_%d  ACC)) y_%d)" % (j,j,i)
				print "       )"
		
		#####################################################################
		# ordering
		#####################################################################


		print "\n       ;; ordering"
		for i in range (1,n):
			for j in range (i+1,n):
				print "       (<= x_%d x_%d)" % (i,j)
		
		print "       (<= door_x_1 door_x_2)"
		print "       (= ordered_1_2 (<= pathx_1 pathx_2))"
		print "       ordered_1_2"
		print "       (or (and (> pathdx_1 0) (> pathdy_1 0)) (and (> pathdx_2 0) (> pathdy_2 0)))"
		
		#####################################################################
		# distances
		#####################################################################
		
		print "\n       ;; distances"
		for i in range (1,n):
			for j in range (i+1,n):
				print "       (ite (> x_%d x_%d) (= DX_%d_%d (- x_%d x_%d)) (= DX_%d_%d (- x_%d x_%d)))" % (i,j,i,j,i,j,i,j,j,i)
				print "       (ite (> y_%d y_%d) (= DY_%d_%d (- y_%d y_%d)) (= DY_%d_%d (- y_%d y_%d)))" % (i,j,i,j,i,j,i,j,j,i)

		print "\n       ;; sum of all distances"
		pairs = [(i,j) for i in range(1,n) for j in range(i+1,n)]
		if len(pairs) == 1:
			print "       (= sum_DX DX_%d_%d)" % (pairs[0][0], pairs[0][1])
			print "       (= sum_DY DY_%d_%d)" % (pairs[0][0], pairs[0][1])
		else:
			print "       (= sum_DX (+ %s))" % (" ".join("DX_%d_%d" % (i,j) for i,j in pairs))
			print "       (= sum_DY (+ %s))" % (" ".join("DY_%d_%d" % (i,j) for i,j in pairs))

			
			
		#####################################################################
		# tables' shapes
		#####################################################################
		
		
		print "\n       ;; tables' shapes"
		print "       (= num_1_1_tables (+"
		for i in range (1,n):
			print "              (ite (<= dx_%d TABLE) 1 0)" % (i)
		print "       ))"
		print "       (= num_2_1_tables (+"
		for i in range (1,n):
			print "              (ite (> dx_%d TABLE) 1 0)" % (i)
		print "       ))"
				
		


		#####################################################################
		# cost: minimize path lenght
		#####################################################################
		
		print "\n       ;; cost function"
		
		if conf == 0:
			print "       (= cost (+ (* 1 sum_DX) (* 1 sum_DY) (* 1 pathdx_1) (* 1 pathdx_2) (* 1 pathdy_1) (* 1 pathdy_2) (* 1 num_1_1_tables) (* 2 num_2_1_tables))"
		else:
			print "       (= cost (+ (* (- 0 1) sum_DX) (* (- 0 1) sum_DY) (* 1 pathdx_1) (* 1 pathdx_2) (* 1 pathdy_1) (* 1 pathdy_2) (* 2 num_1_1_tables) (* 1 num_2_1_tables))"

		print "       )"
		print "	)" 
		print ")"
		print "(maximize cost)"
		print "(check-sat)"
		print "(set-model -1)"
		print "(get-model)"
		print "(exit)"
		
		
	#####################################################################
    # Inference problem SMT2
    #####################################################################	
		
	else:
		# blocks go from 1 to n
		blocks = range (1, n)
		print ";; blocks =", blocks
			
		print "(set-logic QF_LRA)"
		print "(set-option :produce-models true)"
		   
		#####################################################################
		# Variables
		#####################################################################
			
		print "\n;; variable: cost"
		print "(declare-fun cost () Real)"
		print "\n;; variable: delta"
		print "(declare-fun delta () Real)"		
		print "\n;; variable: separation"
		print "(declare-fun separation () Real)"
		
		print "\n;;parameters"
		print "(declare-fun ROOM() Real) ;;lato stanza"
		print "(declare-fun TABLE() Real) ;;lato blocchetto"
		print "(declare-fun ACC() Real) ;; accessibility"
		
		print "\n;; variable: block coordinate"
		for i in blocks:
			print "(declare-fun x_%d () Real)" %i
			print "(declare-fun y_%d () Real)" %i 

		print "\n;; variable: block dimensions"
		for i in blocks:
			print "(declare-fun dx_%d () Real)" %i
			print "(declare-fun dy_%d () Real)" %i

		print "\n;; distances"
		for i in range (1,n):
			for j in range (i+1,n):
				print "(declare-fun DX_%d_%d () Real)" % (i,j)
				
		for i in range (1,n):
			for j in range (i+1,n):
				print "(declare-fun DY_%d_%d () Real)" % (i,j)

		print "(declare-fun sum_DX () Real)"
		print "(declare-fun sum_DY () Real)"
		
		print "\n;;table composition"
		print "(declare-fun num_1_1_tables () Real)"
		print "(declare-fun num_2_1_tables () Real)"
				

		#####################################################################
		# Problem definition
		#####################################################################


		print "\n;; problem"
		print "(assert"
		print "   (and"

		print "\n       ;; constraint: hack"
		print "       (= ROOM 1)"
		print "       (= TABLE %s)" % number_to_smt2(table)
		print "       (= ACC %s)" % number_to_smt2(acc)

		#####################################################################
		# block coordinate constraints 
		#####################################################################

		print "\n       ;; constraint: coordinates"
		for i in blocks:
			print "       (>= x_%d 0) (<= x_%d ROOM)" % (i,i)
			print "       (>= y_%d 0) (<= y_%d ROOM)" % (i,i)
			print "       (or (= dx_%d TABLE)(= dx_%d (* 2 TABLE)))" % (i,i)
			print "       (= dy_%d TABLE)" % (i)
			
		#####################################################################
		# inside image constraints
		#####################################################################

		print "\n       ;; constraint: inside image"
		for i in blocks:
			print "       (<= (+ x_%d dx_%d) ROOM)" % (i,i)
			print "       (<= (+ y_%d dy_%d) ROOM)" % (i,i)

		#####################################################################
		# no overlap constraints
		#####################################################################

		print "\n       ;; constraint: no overlap"
		for i in blocks:
			for j in range(i+1,n):
				print "       ;;overlap_%d_%d" % (i,j)
				print "       (or"
				print "           (<= (+ x_%d (+ dx_%d  ACC)) x_%d)" % (i,i,j)
				print "           (<= (+ y_%d (+ dy_%d  ACC)) y_%d)" % (i,i,j)
				print "           (<= (+ x_%d (+ dx_%d  ACC)) x_%d)" % (j,j,i)
				print "           (<= (+ y_%d (+ dy_%d  ACC)) y_%d)" % (j,j,i)
				print "       )"
		
		#####################################################################
		# ordering
		#####################################################################


		print "\n       ;; ordering"
		for i in range (1,n):
			for j in range (i+1,n):
				print "       (<= x_%d x_%d)" % (i,j)
				
		#####################################################################
		# distances
		#####################################################################
		
		print "\n       ;; distances"
		for i in range (1,n):
			for j in range (i+1,n):
				print "       (ite (> x_%d x_%d) (= DX_%d_%d (- x_%d x_%d)) (= DX_%d_%d (- x_%d x_%d)))" % (i,j,i,j,i,j,i,j,j,i)
				print "       (ite (> y_%d y_%d) (= DY_%d_%d (- y_%d y_%d)) (= DY_%d_%d (- y_%d y_%d)))" % (i,j,i,j,i,j,i,j,j,i)

		print "\n       ;; sum of all distances"
		pairs = [(i,j) for i in range(1,n) for j in range(i+1,n)]
		if len(pairs) == 1:
			print "       (= sum_DX DX_%d_%d)" % (pairs[0][0], pairs[0][1])
			print "       (= sum_DY DY_%d_%d)" % (pairs[0][0], pairs[0][1])
		else:
			print "       (= sum_DX (+ %s))" % (" ".join("DX_%d_%d" % (i,j) for i,j in pairs))
			print "       (= sum_DY (+ %s))" % (" ".join("DY_%d_%d" % (i,j) for i,j in pairs))

		#####################################################################
		# tables' shapes
		#####################################################################
		
		
		print "\n       ;; tables' shapes"
		print "       (= num_1_1_tables (+"
		for i in range (1,n):
			print "              (ite (<= dx_%d TABLE) 1 0)" % (i)
		print "       ))"
		print "       (= num_2_1_tables (+"
		for i in range (1,n):
			print "              (ite (> dx_%d TABLE) 1 0)" % (i)
		print "       ))"
				
		

		
		print "\n       ;; cost function"
		
		if conf == 0:
			print "       (= cost (+ (* 1 sum_DX) (* 1 sum_DY) (* 1 num_1_1_tables) (* 2 num_2_1_tables))"
		else:
			print "       (= cost (+ (* (- 0 1) sum_DX) (* (- 0 1) sum_DY) (* 2 num_1_1_tables) (* 1 num_2_1_tables))"

		print "       )"
		print "	)" 
		print ")"
		print "(maximize cost)"
		print "(check-sat)"
		print "(set-model -1)"
		print "(get-model)"
		print "(exit)"
		

if __name__ == "__main__":
    main()
