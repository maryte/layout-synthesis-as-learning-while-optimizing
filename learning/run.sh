#!/bin/bash

OMT="../../Code/optimathsat/optimathsat"
mkdir -p outputs
mkdir -p inputs

declare -a arr=("lu" "ld" "lr" "dr" "ur")

#for room in 1 2
for room in 2
do
	for doors in "${arr[@]}"
	do 
		for num_blocks in `seq 3 6`
		do
			basename=templates/template_${doors}_${num_blocks}
			echo "running for $basename"
			#Usage: <num blocks> <doors positions={ }> <room size> <template={true||false}
			#generate templates
			python make_template.py $num_blocks $doors $room true > $basename.smt2 #true=template, false=inference problem
			#create training set
		done
	done
done

python generate_inputs.py
python draw_result.py 512