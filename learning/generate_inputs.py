# Generate the input files

import sys
from collections import defaultdict
import glob
from pylmt import LMTRunner
from pylmt import libsmt2float
import numpy as np


C = 1000000
EPSILON = 1e-15
INFTY = 10000
PNG_SIZE = 512
TABLES = 6

def libsmt2float(s):
    parts = [part.strip("() ") for part in s.split()]
    parts = [part for part in parts if len(part)]
    assert parts[0] == "="
    if len(parts) == 3:
        if parts[2].startswith("true"):
            return 1.0
        elif parts[2].startswith("false"):
            return -1.0
        else:
            return float(parts[2])
    elif len(parts) == 5 and parts[2] == "/":
        # example: '(= FEAT_vertical_material (/ 1 2))'
        return float(parts[3]) / float(parts[4])
    elif len(parts) == 6 and parts[2] == "-" and parts[3] == "/":
        # example: '(= separation (- (/ 462499936509899335 81064793292668928)))'
        return -float(parts[4]) / float(parts[5])
    raise NotImplementedError("unhandled assignment string '{}': '{}'".format(s, parts))

def _read_raw_assignments(lines):
    # XXX taken from pylmt/util.py:OptiMathSAT5
    lines = [line for line in lines if not line.startswith("#")]
    if not lines[0] == "sat":
        raise RuntimeError("the problem is unsatisfiable!")
    assignments = {}
    for line in lines[1:]:
        parts = map(str.strip, line.split())
        parts = [part for part in parts if part not in ["(", ")"]]
        if not len(parts):
            continue
        parts[-1] = parts[-1][:-1]
        k, v = parts[0].lstrip("("), " ".join(parts[1:])
        assert sum(c == "(" for c in v) == sum(c == ")" for c in v), \
               "unbalanced parens found in '{}'".format(v)
        assignments[k] = "(= {} {})".format(k, v)
    return assignments
	
def concat(str1,str2):
	if str1=="":
		return str2
	else:
		return str1 +" , "+str2





























































def main():

	MIN_TRAIN_BLOCKS, MAX_TRAIN_BLOCKS, MAX_TEST_BLOCKS = 2, 4, 8
	CONF = ["cafe", "office"]
	DOORS = ["lu","ld","lr","dr","ur"]
	
	# Write out all training and test examples
	#for each configuration
	for conf in CONF:
	
		for doors in DOORS:
		
			
			x_str = ""
			y_str = ""

			print "**********processing '{}_{}' layout**************".format(conf,doors)

			path = "inputs/input_{}_{}.lmt".format(conf,doors)

			with open(path, "w") as out:
			
				#for each number of tables
				for tables in range(3,TABLES+1):
				
					layouts = glob.glob("data/layout_{}_{}_{}_*.model".format(conf,doors,tables))
					for l in layouts:
					
						x_str = ""
						y_str = ""
						
						with open(l) as fp:
							#extract blocks variables from the model
							lines = filter(lambda line: line[0] != "#", map(str.strip, fp.readlines()))
							variable_to_value = _read_raw_assignments(lines)
							data = defaultdict(list)
							for variable, value in sorted(variable_to_value.iteritems()):
								if variable.startswith("x_") or variable.startswith("y_") or \
								   variable.startswith("dx_") or variable.startswith("dy_"):
									#data[variable.split("_")[0]].append(libsmt2float(value))
									data[variable.split("_")[0]].append(value)
							
							x=data["x"]
							y=data["y"]
							dx=data["dx"]
							dy=data["dy"]
							
							for i in range (0,2):
								x_str = concat(x_str,x[i])
								x_str = concat(x_str,y[i])
							for i in range (0,tables):
								y_str = concat(y_str,x[i])
								y_str = concat(y_str,y[i])
								y_str = concat(y_str,dx[i])
								y_str = concat(y_str,dy[i])
							
							mode = "train" if tables <= MAX_TRAIN_BLOCKS else "test"
							out.write("templates/template_{}_{}.smt2 ; {} ; {} ; {}\n".format(doors,tables, x_str, y_str, mode))

								
			# Run LMT proper on the input file we just generated
			lmt = LMTRunner("default", path, C, EPSILON, "optimathsat5", debug=True)
			results = zip(lmt._test_xs, lmt._test_ys, lmt.run())
			print "\n\n\nPRINTING RESULTS"

			r=[]
			with open("RESULT_{}_{}.txt".format(conf,doors), "w") as out:
				for i, (x, y, pred_y) in enumerate(results):
					out.write("\n\npred {}: {}".format(i,pred_y))
					r = np.asarray(pred_y)
					with open("prediction-{}-{}-@{}.txt".format(conf,doors,i), "w") as p:
						p.write("{}".format(r[0]))
					#p =  open("pred_{}.txt".format(i), "w")
					#for n in (0,len(pred_y[0]))
					#	p.write("{}".format(pred_y[0][n]))



		

		# # Draw the predictions
		#for i, (x, y, pred_y) in enumerate(results):
			#draw(path, x, y, pred_y)
			#path = "prediction-{}-{}-@{}.png".format(conf, train_blocks, train_blocks + i + 1)
			
			
		
		#for i, (x, y, pred_y) in enumerate(results):
		#	with open("outputs/pred_{}.out".format(i), "w") as out:
		#		out.write("{}".format(pred_y))
			
		
if __name__ == "__main__":
    main()